<?php
require_once('../commun.php');
//Création de l'instance de userController
$userController = new UserController($connection);
//Traitement du formulaire de login
if(isset($_POST['login'])){
    if(isset($_POST['pseudo']) &&  $_POST['pseudo']!= null){
        if(isset($_POST['password']) && $_POST['password'] != NULL ){
            $pseudo = (string) htmlspecialchars($_POST['pseudo']);
            $password = (string) htmlspecialchars($_POST['password']);
            if($pseudo != null && $password != null){
                $testConnection = $userController->userLogin($pseudo,$password);
                if(isset($testConnection['pseudo'])){
                    $_SESSION['pseudo'] = $pseudo;
                    $_SESSION['id'] = $testConnection['id'];
                    header('Location: ../view/dashboard.php');
                }
                elseif($testConnection == 'failLogin'){
                    header('Location:../view/index.php?erreur_connexion=0');
                    exit();
                }
                else{
                    header('Location:../view/index.php?erreur_connexion=1');
                    exit();
                }
            }else{
                header('Location:../view/index.php?erreur_connexion=0');
                exit();
            }
        }
    }
}
