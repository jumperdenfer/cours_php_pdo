<?php
require_once('../commun.php');
$userController = new UserController($connection);
//Script qui vérifie le tokens d'activation
if(isset($_GET['token'])){
    $token = trim(htmlspecialchars($_GET['token']));
    if($token != null){
        $testTokens = $userController->checkTokens($token);
        if($testTokens == true){
            echo 'Votre compte a bien été activé';
        }
        else{
            echo 'Une erreur est survenue avec ce Token';
        }
    }
}
