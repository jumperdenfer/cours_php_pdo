<?php
//Script qui s'occupe de l'enregistrement d'un utilisateur
require_once('../commun.php');
$userController = new UserController($connection);
//Vérifie le formulaire
if(isset($_POST['register'])){
    $profil['pseudo'] = filter_input(INPUT_POST,'pseudo',FILTER_SANITIZE_STRING);
    if(isset($_POST['password'])){
        //Exemple à décommenter si utilisation de hash;
        $profil['password'] = password_hash($_POST['password'],PASSWORD_DEFAULT);
        //$profil['password']= trim(htmlspecialchars($_POST['password']));
    }
    $profil['mail'] = filter_input(INPUT_POST,'mail',FILTER_VALIDATE_EMAIL);
    $profil['age'] = filter_input(INPUT_POST,'age',FILTER_VALIDATE_INT);
    var_dump($profil['mail']);
    var_dump($_POST);
    //si tout les chamsp sont remplie
    if($profil['pseudo'] != null && $profil['password'] != null && $profil['mail'] != null && $profil['age'] != null){
        //Créé un tokens
        $length = 20;
        //Définie les characters à utiliser afin de générais le token
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        //Compte la longueur de charactères
        $charactersLength = strlen($characters);
        //Variable qui vas stocker le token
        $randomString = '';
        //Boucle for sur la taille définie de lenght
        for ($i = 0; $i < $length; $i++) {
            //Ajout à randomstring un charactère
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        //passe randomString à token
        $token = $randomString;
        $profil['token'] = $token;
        //Ajoute l'utilisateur
        $testAdd = $userController->addUser($profil);
        //Si l'enregitrement ) fonctionner, envoie le mail à l'utilisateur
        if($testAdd == true){
            //vus que le site est en local, il faut utiliser le liens dans user token
            $message = 'Pour terminer votre inscription sur XXX veuillez suivre le liens suivant XXX/token.php?token='.$token.'&add=true';
            $to = $profil['mail'];
            $headers = 'From: benoitlagrange38@hotmail.fr';
            $headers.= 'X-Mailer: PHP/' . phpversion();
            $headers.= 'Content-type: text/html; charset=utf-8';
            $subject = 'Inscription site XXX';
            $body  = wordwrap($message, 70, "\r\n");
            mail($to, $subject, $body, $headers);
            header('Location:../view/index.php');
            exit();
        }else{
            header('Location:../view/index.php?error=erreur_creation');
            exit();
        }
    }
}
