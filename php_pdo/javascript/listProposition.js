//Gestion des propositions validé par l'utilisateur courrant ou d'autre utilisateur
let generateListeProposition = async()=>{
    let data = await getAllProposition();
    let i = 1
    let tableau = document.getElementById('allProposition')
    for(let proposition of data){
        //Créé les pourcentage
        let totalVote = parseInt(proposition['vote_pour']) + parseInt(proposition['vote_contre']);
        let totalPour = proposition['vote_pour'] * 100 / totalVote;
        let totalContre = proposition['vote_contre'] * 100 / totalVote;
        console.log(totalVote);
        console.log(totalPour);
        console.log(totalContre);
        //Créé la node du tableau
        let trNode = document.createElement('tr');
        trNode.innerHTML = `
            <td>${i}</td>
            <td>${proposition.nom}</td>
            <td><a href="./proposition.php?xtrid=${proposition.id}">Voir</a></td>
            <td>${proposition.pseudo}</td>
            <td>${totalPour.toFixed(2)}%</td>
            <td>${totalContre.toFixed(2)}%</td>
        `
        tableau.append(trNode);
        i++
    }
}
generateListeProposition();
