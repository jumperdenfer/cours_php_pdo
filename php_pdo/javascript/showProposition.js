//Créé le template
let showProposition = async()=>{
    let data = await getOneProposition(idProposition);
    //Récupère les zone de l'html qui doivent être modifier
    let titleArea = document.getElementById('titleProposition');
    let dateArea = document.getElementById('date');
    let descriptionArea = document.getElementById('description');
    let authorArea = document.getElementById('author');
    let buttonArea = document.getElementById('boutonArea');
    let commentaireArea = document.getElementById('commentaires');
    let date = new Date(data.date_ajout)
    date = date.toLocaleDateString('fr-FR');

    //Vérifie si l'utilisateur à voté si oui lui indique, sinon lui propose de voté
    let voteDiv = `
            <div>
                <div class="">
                    <button id="votePour" class="btn btn-primary">Voter Pour</button>
                </div>
                <div class="mt-2">
                    <button id="voteContre" class="btn btn-danger">Voter contre</button>
                </div>
            </div>
        `;

    if(data['vote'] == true){
        voteDiv = 'Vous avez déjà voté.';
    }

    //Ajout des valeurs dans le html
    titleArea.innerHTML = data['nom'];
    authorArea.innerHTML = data['author'];
    dateArea.innerHTML = date;
    descriptionArea.innerHTML = `<p>${data['description']}</p>`
    buttonArea.innerHTML = `${voteDiv}`;
    //affichage des commentaire
    for(let commentaire of data['commentaire']){
        let nodeCommentaire = document.createElement('div');
        nodeCommentaire.innerHTML = `
            <hr>
            <p>${commentaire.pseudo}</p>
            <p>${commentaire.commentaire}</p>
            <p> ${commentaire.updated_at ? 'Modifier le: '+commentaire.updated_at : 'créé le: '+commentaire.created_at }</p>
            <button class="btn btn-primary" data-toggle="modal" data-target="#modal-${commentaire.id}" >Modifier</button>
            <button onClick='delCom(${commentaire.id})' class="btn btn-danger">Supprimer</button>
            <div>
                <div id="modal-${commentaire.id}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <form class="container p-1" method="post" action="../proposition/propositionGestion.php">
                                <div class="form-group mt-1">
                                    <p>Commentaire:</p>
                                    <textarea cols="60" name="commentaire" id="com_${commentaire.id}">${commentaire.commentaire ? commentaire.commentaire : ''}</textarea>
                                </div>
                                <div class="mt-1">
                                    <input type="hidden" name="idCommentaire" value="${commentaire.id}">
                                    <input type="hidden" name="idProposition" value="${idProposition}">
                                    <input class="btn btn-primary" type="submit" name="commentaireUpdate" value="modifier">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
                                    `
        commentaireArea.append(nodeCommentaire);
    }
}
//Fonction de vote pour les boutons
let buttonBind = ()=>{
    let buttonPour = document.getElementById('votePour');
    let buttonContre = document.getElementById('voteContre');
    if(buttonPour != null){
        buttonPour.addEventListener('click',function(){
            voteFor(idProposition,0);
        })
        buttonContre.addEventListener('click',function(){
            voteFor(idProposition,1);
        })
    }
}
//Fonction spécifique pour la suppression d'un commentaire
let delCom = (id)=>{
    let confirmDel =  confirm('Voulez vous vraiment supprimer ce commentaire ?')
    if(confirmDel == true){
        deletingCommentaire(id);
    }
}
//Initialise l'affichage
let initProposition = async()=>{
    await showProposition();
    buttonBind();
}
initProposition();
