//Gestion des boutons
let modalModify = async (id,desc,nom)=>{
    let precedenteModal = document.getElementById(`modalDiv_${id}`);
    if(precedenteModal != null){
        precedenteModal.parentNode.removeChild(precedenteModal);
        console.log('one deleted')
    }
    let body = document.getElementById('body');
    let nodeModal = document.createElement('div')
    nodeModal.id = `modalDiv_${id}`;
    nodeModal.innerHTML =
        `
        <div id="modal-${id}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form class="container" method="post" action="../proposition/propositionGestion.php">
                    <div class="form-group mt-1">
                        <label for="nom_${id}">Nom de la proposition</label>
                        <input id="nom_${id}" class="form-control" type="text" name="nomProposition" value="${nom}">
                    </div>
                    <div class="form-group">
                        <label for="desc_${id}">Description de la proposition</label>
                        <textarea name="descProposition" id="desc_${id}" class="form-control">${desc}</textarea
                    </div>
                    <div class="mt-1">
                        <input type="hidden" name="idProposition" value="${id}">
                        <input class="btn btn-primary" type="submit" name="propositionUpdate" value="modifier">
                    </div>
                </form>
            </div>
          </div>
        </div>
        `
    await body.append(nodeModal);

    $(`#modal-${id}`).modal('show');


}
let validateProposition = async(id)=>{
    let buttonValidate = document.getElementById(`validate_${id}`);
    let buttonDelete = document.getElementById(`delete_${id}`);
    let buttonModifier = document.getElementById(`modifier_${id}`);
    buttonValidate.disabled = true;
    buttonDelete.disabled = true;
    buttonModifier.disabled = true;
    let result = await validatingProposition(id)
    if(result == true){
        alert('La proposition à etait validé');
        window.location.reload(true);
    }
    else{
        alert('Une erreur est survenue');
        buttonValidate.disabled = false;
        buttonDelete.disabled = false;
        buttonModifier.disabled = false;
    }
}
let deleteProposition = async(id)=>{
    //Récupère et désactive les boutons
    let buttonValidate = document.getElementById(`validate_${id}`);
    let buttonDelete = document.getElementById(`delete_${id}`);
    let buttonModifier = document.getElementById(`modifier_${id}`);
    buttonValidate.disabled = true;
    buttonDelete.disabled = true;
    buttonModifier.disabled = true;
    let deleteRequest = await deletingProposition(id);
    if(deleteRequest == true){
        alert('La proposition a bien été supprimer');
        window.location.reload(true)
    }
    else{
        alert('Une erreur est survenue');
        buttonValidate.disabled = false;
        buttonDelete.disabled = false;
        buttonModifier.disabled = false;
    }
}
let recupUserProposition = async() => {
    let listeProposition = await getUserList();
    let divUserProposition = document.getElementById('userProposition');
    let iNum = 1
    for(let proposition of listeProposition){
        let buttonModify = `<button onClick="modalModify('${proposition.id}','${proposition.description}','${proposition.nom}')"' class="btn btn-primary" id="modifier_${proposition.id}">Modifier</button>`;
        let buttonValidate = `<button id="validate_${proposition.id}" class="btn btn-success" onClick='validateProposition(${proposition.id})'>Validation</button>`;
        let buttonDelete = `<button id="delete_${proposition.id}" class="btn btn-danger" onClick='deleteProposition(${proposition.id})'>Supprimer</button>`
        if(proposition.validate != 0){
            buttonModify = `<button class="btn btn-secondary" disabled>Modifier</button>`;
            buttonValidate = `<button class="btn btn-secondary" disabled>Validation</button>`
            buttonDelete = `<button class=" btn btn-secondary" disabled>Supprimer</button>`
        }
        let nodeFormProposition = document.createElement('tr');
        nodeFormProposition.innerHTML =
        `
            <th scope='row'>${iNum}</th>
            <td>${proposition.nom}</td>
            <td class="mw-100">${proposition.description}</td>
            <td>${buttonModify}</td>
            <td>${buttonValidate}</td>
            <td>${buttonDelete}</td>
        `
        divUserProposition.append(nodeFormProposition);
        iNum++
    }
}
//Initialise la récupération des proposition
recupUserProposition();
