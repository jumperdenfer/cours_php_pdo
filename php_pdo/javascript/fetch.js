let urlGet = '../proposition/propositionJSON.php';
let urlPost = '../proposition/propositionGestion.php';
//Récupère la liste des proposition de l'utilisateur actuel
let getUserList = ()=>{
    return new Promise(function(resolve, reject) {
        resolve(
            fetch(urlGet,{
                method: 'POST',
                headers: {'Content-Type' : 'JSON'},
                body: JSON.stringify({'getUserProposition' : true})
            })
            .then((res)=>{return res.json()})
            .then((json)=>{return listData = json})
        )
    });
}
//Envoie la validation d'un bouton
let validatingProposition = (id)=>{
    return new Promise(function(resolve, reject) {
        let objProposition = {};
        objProposition['id'] = parseInt(id);
        resolve(
            fetch(urlPost,{
                method: 'POST',
                headers: {'Content-Type' : 'JSON'},
                body: JSON.stringify({"sendValidate" : objProposition})
            })
            .then((res)=>{return res.json()})
            .then((json)=>{return json})
        )
    });
}
//Envoie la demande de suppression d'une proposition
let deletingProposition = (id) =>{
    return new Promise(function(resolve, reject) {
        let objProposition = {};
        objProposition['id'] = id;
        resolve(
            fetch(urlPost,{
                method: 'POST',
                headers: {'Content-Type' : 'JSON'},
                body: JSON.stringify({'sendDelete':objProposition})
            })
            .then((res)=>{return res.json()})
            .then((json)=>{return json})
        )
    });
}
//Permet de récupérais la liste des toutes les propositions qui sont en état " 1"
let getAllProposition = ()=>{
    return new Promise(function(resolve, reject) {
        resolve(
            fetch(urlGet,{
                method: "POST",
                headers: {'Content-Type' : 'JSON'},
                body: JSON.stringify({'getAllProposition' : true})
            })
            .then((res)=>{return res.json()})
            .then((json)=>{return listData = json})
        )
    });
}
//Permet d'affiché une proposition
let getOneProposition = (id)=>{
    return new Promise(function(resolve, reject) {
        let objProposition = {};
        objProposition['id'] = id;
        resolve(
            fetch(urlGet,{
                method: 'POST',
                headers: {'Content-Type' : 'JSON'},
                body: JSON.stringify({'getOneProposition' : objProposition})
            })
            .then((res)=>{return res.json()})
            .then((json)=>{return listData = json})
        )
    });
}
//Permet de voté pour un proposition
let voteFor = (id,vote)=>{
    return new Promise(function(resolve, reject) {
        let objVote = {};
        objVote['id_proposition'] = id;
        objVote['vote'] = vote;
        resolve(
            fetch(urlPost,{
                method: 'POST',
                headers : {'Content-Type' : 'JSON'},
                body: JSON.stringify({'setVote' : objVote})
            })
            .then((res)=>{return res.json()})
            .then((json)=>{
                if(json == 'true'){
                    alert('Votre vote a été pris en compte');
                    window.location.reload();
                }
                else{
                    alert('Une erreur est survenue durant l\'enregistrement de votre vote');
                    window.location.reload(true);
                }
            })
        )
    });
}
//Permet de supprimer un commentaire
let deletingCommentaire = (id)=>{
    return new Promise(function(resolve) {
        let objCom = {};
        objCom['id'] = parseInt(id);
        resolve(
            fetch(urlPost,{
                method: 'POST',
                headers: {'Content-Type' : 'JSON'},
                body: JSON.stringify({'deleteCom' : objCom})
            })
            .then((res)=>{return res.json()})
            .then((json)=>{
                if(json == true){
                    alert('Le commentaire a été supprimé');
                    window.location.reload();
                }
                else{
                    alert('Une erreur en base de données est survenue');
                }
            })
        )
    });

}
