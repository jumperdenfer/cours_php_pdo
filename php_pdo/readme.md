# README
## Projet PHP pdo
Arborescence des fichiers

 - Controller
	 - commentaireController.php
	 - propositionController.php
	 - userController.php
	 - voteController.php
 - javascript (folder)
	 - fetch.js
	 - listProposition.js
	 - showProposition.js
	 - userProposition.js
 - proposition (folder)
	 - propositionGestion.php
	 - propositionJSON.php
 - user (folder)
	 - register.php
	 - token.php
	 - traitementLogin.php
 - view (folder)
	 - include_html (folder)
		 - head.inc.html
		 - javascript.inc.html
	 - dashboard.php
	 - index.php
	 - proposition.php
 - commun.php
 - configuration.php
 - index.php
 - readme.md

## Objectif du projet:
Réaliser un site permettant aux utilisateurs de réaliser des propositions,de voté pour ces dernières et de laisser des commentaires.
Les techno utiliser sont :

 - php
 - javascript
 - html
 - css
 - boostrap (framework css)

Le site permet aux utilisateurs de s'inscrire et de se connecter, une fois ces étapes effectuer il accède à l'interface utilisateur ( dashboard) depuis laquelle ils peuvent créé des propositions, les modifier, les valider ou le supprimer.
Dans chaque propositions, ils peuvent voté pour ou contre, et laisser un ou plusieurs commentaires.
