<?php
require_once('../commun.php');
if(!isset($_SESSION['pseudo'])){
    header('Location:../view/index.php');
}
$pseudo = $_SESSION['pseudo'];
$idUser = $_SESSION['id'];
$propositionController = new PropositionController($connection);
$voteController = new VoteController($connection);
$commentaireController = new CommentaireController($connection);
//Script qui s'occupe de gérais le traitement des propostion
if(isset($_POST['addProposition'])){
    $nomProposition;
    $descProposition;
    //Vérifie le nom de la proposition
    if(isset($_POST['nom'])){
        $nomProposition = (string) htmlspecialchars($_POST['nom']);
    }
    //Vérifie le texte de la proposition
    if(isset($_POST['desc'])){
        $descProposition = (string) htmlspecialchars($_POST['desc']);
    }
    if($nomProposition != null && $descProposition != null){

        $sendProposition = $propositionController->createProposition($nomProposition,$descProposition,$idUser);
        if(!is_array($sendProposition)){
            $lastIdProposition = $sendProposition;
            $addVote = $voteController->addVote($idUser,$lastIdProposition,0);
            header('Location:../view/dashboard.php?dashboard=add');
            exit();
        }
        else{
            //Renvoie avec l'erreur provenant du traitement en base de données de la requête
            header('Location:../view/dashboard.php?dashboard=errorBdd');
            exit();
        }
    }
    else{
        //Renvoie sur la page d'ajout indiquant une erreur de forumulaire
        header('Location:../view/dashboard.php?proposition=errorForm');
    }
}
//Script qui s'occupe de modifier une propositions
if(isset($_POST['propositionUpdate'])){
    $nomProposition = filter_input(INPUT_POST,'nomProposition',FILTER_SANITIZE_SPECIAL_CHARS);
    $descProposition = filter_input(INPUT_POST,'descProposition',FILTER_SANITIZE_SPECIAL_CHARS);
    $idProposition = filter_input(INPUT_POST,'idProposition',FILTER_VALIDATE_INT);
    if($nomProposition != null && $descProposition != null && $idProposition != null){
        $testSend = $propositionController->updateProposition($nomProposition,$descProposition,$idProposition,$idUser);
        if($testSend == true){
            header('Location:../view/dashboard.php?dashboard=update');
        }
        else{
            //Renvoie avec l'erreur provenant du traitement en base de données de la requête
            header('Location:../view/da.php?dashboard=errorBdd');
        }
    }
    else{
        header('Location:../view/dashboard.php?proposition=errorForm');
    }
}
//Script qui s'occupe d'enregistré un commentaire
if(isset($_POST['sendCommentaire'])){
    $commentaire = trim(htmlspecialchars($_POST['commentaire']));
    $idProposition = (int) trim(htmlspecialchars($_POST['idProposition']));
    if($commentaire != null && $idProposition != null){
        $testSend = $commentaireController->addCommentaire($idProposition,$idUser,$commentaire);
        if($testSend){
            header('Location:../view/proposition.php?xtrid='.$idProposition);
        }else{
            header('Location:../view/proposition.php?xtrid='.$idProposition.'&error=1');
        }
    }else{
        header('Location:../view/proposition.php?xtrid='.$idProposition.'&error=2');
    }
}
//Script qui permet de modifier un commentaire
if(isset($_POST['commentaireUpdate'])){
    $commentaire = trim(htmlspecialchars($_POST['commentaire']));
    $idCommentaire = trim(htmlspecialchars($_POST['idCommentaire']));
    $idProposition = trim(htmlspecialchars($_POST['idProposition']));
    if($commentaire != null && $idCommentaire != null){
        $testSend = $commentaireController->updateCommentaire($idCommentaire,$commentaire,$idUser);
        if($testSend == true){
            header('Location:../view/proposition.php?xtrid='.$idProposition);
        }else{
            header('Location:../view/proposition.php?xtrid='.$idProposition.'&error=1');
        }
    }else{
        header('Location:../view/proposition.php?xtrid='.$idProposition.'&error=2');
    }
}
//Traitement des requête en fetch
$json_str = file_get_contents('php://input');
$requeteFetch = json_decode($json_str,true);
if(isset($requeteFetch['sendValidate'])){
    $idValidation = (int) htmlspecialchars($requeteFetch['sendValidate']['id']);
    if($idValidation != null){
        $testValidate = $propositionController->validateProposition($idValidation,$idUser);
        if($testValidate == true){
            echo json_encode(true);
        }
        else{
            echo json_encode($testValidate);
        }
    }
}
if(isset($requeteFetch['sendDelete'])){
    $id = (int) trim(htmlspecialchars($requeteFetch['sendDelete']['id']));
    if($id != null){
        $testDelete = $propositionController->deleteProposition($id,$idUser);
        if($testDelete == true){
            echo json_encode(true);
        }else{
            echo json_encode($testDelete);
        }
    }
}
if(isset($requeteFetch['setVote'])){
    $idProposition = (int) trim(htmlspecialchars($requeteFetch['setVote']['id_proposition']));
    $vote = (int) trim(htmlspecialchars($requeteFetch['setVote']['vote']));
    if($idProposition != null && ($vote != null ||$vote == 0)){
        $testVote = $voteController->addVote($idUser,$idProposition,$vote);
        if($testVote == true){
            echo json_encode('true');
        }else{
            echo json_encode('false');
        }
    }
}
if(isset($requeteFetch['deleteCom'])){
    $idCom = (int) trim(htmlspecialchars($requeteFetch['deleteCom']['id']));
    if($idCom != null){
        $testDel = $commentaireController->deleteCommentaire($idCom,$idUser);
        echo json_encode($testDel);
    }
}
