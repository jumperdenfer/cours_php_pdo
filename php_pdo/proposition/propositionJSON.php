<?php
require_once('../commun.php');
//Script permettant de renvoyé au format JSON la liste des propositions
if(!isset($_SESSION['pseudo'])){
    header('Location:../view/index.php');
}
$idUser = $_SESSION['id'];

$propositionController = new PropositionController($connection);
$voteController = new VoteController($connection);
$commentaireController = new CommentaireController($connection);
//traitement des requête
$json_str = file_get_contents('php://input');
$requeteFetch = json_decode($json_str,true);
if(isset($requeteFetch['getUserProposition'])){
    $listUserProposition = $propositionController->getUserProposition($idUser);
    echo json_encode($listUserProposition);
}
if(isset($requeteFetch['getAllProposition'])){
    $listProposition = $propositionController->getAllProposition();
    echo json_encode($listProposition);
}
if(isset($requeteFetch['getOneProposition'])){
    $idProposition = (int) trim(htmlspecialchars($requeteFetch['getOneProposition']['id']));
    $proposition = $propositionController->getOneProposition($idProposition);
    $checkProposition = $voteController->checkVote($idUser,$idProposition);
    $listCommentaire = $commentaireController->showAllCommentaire($idProposition);
    if($proposition != null){
        $proposition['vote'] = $checkProposition;
        $proposition['commentaire'] = $listCommentaire;
        echo json_encode($proposition);
    }
}
