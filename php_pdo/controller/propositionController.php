<?php
class PropositionController{
    Private $hote;
    Private $db_user;
    Private $db_pass;
    public function __construct($connection){
        $this->hote = $connection['host'];
        $this->db_user = $connection['dbUser'];
        $this->db_pass = $connection['dbPass'];
    }
    Public function db_connect(){
        try{
            $db = new PDO($this->hote,$this->db_user,$this->db_pass);
            return $db;
        }catch(Exception $e){
            header('Location:../view/index.php?erreur_connexion=1');
            exit();
        }
    }
    /**
    * Function createProposition
    * permet l'enregistrement d'une proposition en base de données
    * @param STRING nom de la proposition
    * @param STRING description de la proposition
    * @return BOOLEAN
    */
    Public function createProposition($nomProposition,$descProposition,$idUser){
        $db = $this->db_connect();
        $params['nom'] = $nomProposition;
        $params['description'] = $descProposition;
        $params['utilisateur'] = $idUser;
        $request = $db->prepare("INSERT INTO proposition SET nom = :nom,
                                                            description = :description,
                                                            id_user = :utilisateur,
                                                            date_ajout = CURRENT_TIMESTAMP");
        $request->bindValue(':nom',$params['nom']);
        $request->bindValue(':description',$params['description']);
        $request->bindValue(':utilisateur',$params['utilisateur']);
        $testReq = $request->execute();
        $error = $request->errorInfo();
        $request->closeCursor();
        if($testReq == true){
            return $db->lastInsertId();
        }else{
            var_dump($error);
        }
    }
    /**
    * Fonction getUserProposition
    * Permet de récupérais toute les propositions d'un utilisateur
    * @param INT id de l'utilisateur
    * @return ARRAY la liste des propositions de l'utilisateur
    */
    Public function getUserProposition($idUser){
        $db = $this->db_connect();
        $params['id_user'] = (int) $idUser;
        $request = $db->prepare('SELECT * FROM proposition WHERE id_user = :id_user');
        $request->bindValue(':id_user',$params['id_user']);
        $testReq = $request->execute();
        $data = $request->fetchAll(PDO::FETCH_ASSOC);
        $error = $request->errorInfo();
        $request->closeCursor();
        if($testReq == true){
            return $data;
        }else{
            return $error;
        }
    }
    /**
    * function updateProposition
    * Permet de mettre à jour une proposition
    * @param STRING nom de la proposition
    * @param STRING description de la proposition
    * @param INT ID de la proposition
    */
    Public function updateProposition($nomProposition,$descProposition,$idProposition,$idUser){
        $db = $this->db_connect();
        $params['nom'] = (string) $nomProposition;
        $params['description'] = (string) $descProposition;
        $params['id'] = (int) $idProposition;
        $params['id_user'] = (int) $id_user;
        $request = $db->prepare('UPDATE proposition SET nom = :nom, description = :description WHERE id = :id  AND validate = 0 AND id_user = :id_user');
        $request->bindValue(':nom',$params['nom']);
        $request->bindValue(':description',$params['description']);
        $request->bindValue(':id',$params['id']);
        $request->bindValue(':id_user',$params['id_user']);
        $testReq = $request->execute();
        $error = $request->errorInfo();
        if($testReq == true){
            return true;
        }
        else{
            return $error;
        }
    }
    /**
    * function validateProposition
    * Permet de passer en validé une proposition
    */
    Public function validateProposition($idProposition,$idUser){
        $db = $this->db_connect();
        $params['id'] = (int) $idProposition;
        $params['id_user'] = (int) $idUser;
        $request = $db->prepare('UPDATE proposition SET validate = 1 WHERE id = :id AND id_user = :id_user');
        $request->bindValue(':id',$params['id']);
        $request->bindValue(':id_user',$params['id_user']);
        $testReq = $request->execute();
        $error = $request->errorInfo();
        $request->closeCursor();
        if($testReq == true){
            return true;
        }
        else{
            return $error;
        }
    }
    /**
    * Public function deleteProposition
    * Permet de supprimer une proposition non validé
    */
    Public function deleteProposition($idProposition,$idUser){
        $db = $this->db_connect();
        $params['id'] = (int) $idProposition;
        $params['id_user'] = (int) $idUser;
        $request = $db->prepare('DELETE FROM proposition WHERE id = :id AND id_user = :id_user');
        $request->bindValue(':id',$params['id']);
        $request->bindValue(':id_user',$params['id_user']);
        $testReq = $request->execute();
        $error = $request->errorInfo();
        if($testReq == true){
            return true;
        }
        else{
            return $error;
        }
    }
    /**
    * fonction getAllProposition
    * Permet de récupérais la liste de toutes les propositions validé
    */
    Public function getAllProposition(){
        $db = $this->db_connect();
        $request = $db->prepare('   SELECT p.*,u.pseudo
                                    FROM proposition p
                                    JOIN utilisateur u ON p.id_user = u.id
                                    WHERE validate = 1');
        $testReq = $request->execute();
        $data = $request->fetchAll(PDO::FETCH_ASSOC);
        $error = $request->errorInfo();
        if($testReq == true){
            return $data;
        }
        else{
            return $error;
        }
    }
    /**
    * Fonction getOneProposition()
    * Permet d'affiché une proposition depuis sont id
    * @param INT ID de la proposition
    */
    Public function getOneProposition($id){
        $db = $this->db_connect();
        $params['id'] = (int) $id;
        $request = $db->prepare('   SELECT  p.nom,
                                            p.description,
                                            p.date_ajout,
                                            p.vote_pour,
                                            p.vote_contre,
                                            u.pseudo as author
                                    FROM proposition p
                                    JOIN utilisateur u ON u.id = p.id_user
                                    WHERE p.id = :id');
        $request->bindValue(':id',$params['id']);
        $testReq = $request->execute();
        $error = $request->errorInfo();
        $data = $request->fetch(PDO::FETCH_ASSOC);
        if($testReq == true){
            return $data;
        }
        else{
            return $error;
        }
    }
}
