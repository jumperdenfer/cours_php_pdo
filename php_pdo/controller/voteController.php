<?php

class VoteController{
    Private $hote;
    Private $db_user;
    Private $db_pass;
    public function __construct($connection){
        $this->hote = $connection['host'];
        $this->db_user = $connection['dbUser'];
        $this->db_pass = $connection['dbPass'];
    }

    Public function db_connect(){
        try{
            $db = new PDO($this->hote,$this->db_user,$this->db_pass);
            return $db;
        }catch(Exception $e){
            header('Location:../view/index.php?erreur_connexion=1');
            exit();
        }
    }
    /**
    * Fonction qui permet d'enregistrer le vote d'un utilisateur
    * @param INT id de l'utilisateur
    * @param INT id de la proposition
    * @param INT type du vote 0 = pour, 1 = contre;
    */
    Public function addVote($idUser,$idProposition,$vote){
        $db = $this->db_connect();
        $params['id_user'] = $idUser;
        $params['id_proposition'] = $idProposition;
        $params['type'] = (int) $vote;
        $voteOk = null;
        $request = $db->prepare('   INSERT INTO vote
                                    SET utilisateur = :utilisateur,
                                        proposition = :proposition,
                                        date_vote = CURRENT_TIMESTAMP,
                                        type = :type
                                ');
        $request->bindValue(':utilisateur',$params['id_user']);
        $request->bindValue(':proposition',$params['id_proposition']);
        $request->bindValue(':type',$params['type']);
        $testReq = $request->execute();
        $error[0] = $request->errorInfo();
        if($testReq == true){
            $voteOk = true;
        }
        else{
            return $error;
        }
        //Gère le vote pour
        if($voteOk == true && $vote == 0){
            $request = $db->prepare('SELECT vote_pour FROM proposition WHERE id = :id');
            $request->bindValue(':id',$params['id_proposition']);
            $request->execute();
            $donnees = $request->fetch(PDO::FETCH_ASSOC);
            $request->closeCursor();
            $currentVote = $donnees['vote_pour'] +1;

            $request = $db->prepare('UPDATE proposition SET vote_pour = :vote_pour WHERE id = :id');
            $request->bindValue(':id',$params['id_proposition']);
            $request->bindValue(':vote_pour',$currentVote);

            $testReq = $request->execute();
            $error = $request->errorInfo();
            if($testReq == true){
                return true;
            }else{
                return $error;
            }
        }
        //Gère le vote contre
        if($voteOk == true && $vote == 1){
            $request = $db->prepare('SELECT vote_contre FROM proposition WHERE id = :id');
            $request->bindValue(':id',$params['id_proposition']);
            $request->execute();
            $donnees = $request->fetch(PDO::FETCH_ASSOC);
            $request->closeCursor();
            $currentVote = $donnees['vote_contre'] +1;

            $request = $db->prepare('UPDATE proposition SET vote_contre = :vote_contre WHERE id = :id');
            $request->bindValue(':id',$params['id_proposition']);
            $request->bindValue(':vote_contre',$currentVote);
            $testReq = $request->execute();
            $error = $request->errorInfo();
            if($testReq == true){
                return true;
            }
            else{
                return $error;
            }
        }
    }
    /**
    * Fonction qui vérifie si l'utilisateur à voté pour la proposition
    * @param INT id de l'utilisateur
    * @param INT id de la proposition
    * @return BOOLEAN En fonction de l'existence d'un vote
    */
    Public function checkVote($idUser,$idProposition){
        $db = $this->db_connect();
        $params['id_user'] = $idUser;
        $params['id_proposition'] = $idProposition;
        $request = $db->prepare("SELECT * FROM vote WHERE utilisateur = :utilisateur AND proposition = :proposition");
        $request->bindValue('utilisateur',$params['id_user']);
        $request->bindValue('proposition',$params['id_proposition']);
        $testReq = $request->execute();
        $data = $request->fetchAll(PDO::FETCH_ASSOC);
        if(count($data) != 0){
            return true;
        }
        else{
            return false;
        }
    }
}
