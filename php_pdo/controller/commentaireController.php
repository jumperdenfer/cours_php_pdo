<?php
class CommentaireController{
    Private $hote;
    Private $db_user;
    Private $db_pass;
    public function __construct($connection){
        $this->hote = $connection['host'];
        $this->db_user = $connection['dbUser'];
        $this->db_pass = $connection['dbPass'];
    }
    Public function db_connect(){
        try{
            $db = new PDO($this->hote,$this->db_user,$this->db_pass);
            return $db;
        }catch(Exception $e){
            header('Location:../view/index.php?erreur_connexion=1');
            exit();
        }
    }
    /**
    * Fonction addCommentaire
    * Permet d'enregistré un commentaire
    * @param INT id de la proposition
    * @param INT id de l'utilisateur
    * @param STRING commentaire de l'utilisateur
    */
    Public function addCommentaire($idProposition,$idUtilisateur,$commentaire){
        $db = $this->db_connect();
        $params['id_proposition'] = (int) $idProposition;
        $params['id_utilisateur'] = (int) $idUtilisateur;
        $params['commentaire'] = (string) $commentaire;
        $request = $db->prepare('   INSERT INTO commentaire
                                    SET id_proposition = :id_proposition,
                                        id_utilisateur = :id_utilisateur,
                                        commentaire = :commentaire
                                ');
        $request->bindParam(':id_proposition',$params['id_proposition']);
        $request->bindParam(':id_utilisateur',$params['id_utilisateur']);
        $request->bindParam(':commentaire',$params['commentaire']);
        $testReq = $request->execute();
        $request->closeCursor();
        if($testReq == true){
            return true;
        }else{
            return $error;
        }
    }
    /**
    * Fonction showAllCommentaire
    * Affiche les commentaire d'une proposition
    * @param INT id de la proposition
    * @return ARRAY liste des proposition
    */
    public function showAllCommentaire($idProposition){
        $db = $this->db_connect();
        $params['id_proposition'] = (int) $idProposition;
        $request = $db->prepare('   SELECT  c.commentaire,
                                            c.id,
                                            c.created_at,
                                            c.updated_at,
                                            u.pseudo
                                    FROM commentaire c
                                    JOIN utilisateur u ON u.id = c.id_utilisateur
                                    WHERE id_proposition = :id_proposition
                                    ORDER BY created_at');
        $request->bindParam(':id_proposition',$params['id_proposition']);
        $testReq = $request->execute();
        $error = $request->errorInfo();
        $commentaire = $request->fetchAll(PDO::FETCH_ASSOC);
        if($testReq == true){
            return $commentaire;
        }else{
            return $error;
        }

    }
    /**
    * Fonction updateCommentaire
    * Permet de mettre à jour un commentaire
    * @param INT id du commentaire
    * @param STRING commentaire de l'utilisateur
    * @param INT id de l'utilisateur
    * @return BOOLEAN Peut retourné un code d'erreur SQL
    */
    Public function updateCommentaire($idCommentaire,$commentaire,$idUser){
        $db = $this->db_connect();
        $params['id'] = $idCommentaire;
        $params['commentaire'] = $commentaire;
        $request = $db->prepare("   UPDATE commentaire
                                    SET commentaire = :commentaire,
                                        updated_at = CURRENT_TIMESTAMP
                                    WHERE id = :id");
        $request->bindParam('id',$params['id']);
        $request->bindParam('commentaire',$params['commentaire']);
        $testReq = $request->execute();
        $error = $request->errorInfo();
        $request->closeCursor();
        if($testReq == true){
            return true;
        }
        else{
            return $error;
        }
    }
    /**
    * Fonction deleteCommentaire
    * Permet de supprimer un commentaire
    * @param INT id du commentaire
    * @param INT id de l'utilisateur
    */
    Public function deleteCommentaire($idCommentaire,$idUser){
        $db = $this->db_connect();
        $params['id'] = (int) $idCommentaire;
        $params['user'] = (int) $idUser;
        $request = $db->prepare('   DELETE FROM commentaire
                                    WHERE id = :id
                                        AND id_utilisateur = :id_utilisateur ');
        $request->bindParam(':id',$params['id']);
        $request->bindParam(':id_utilisateur',$params['user']);
        $testReq = $request->execute();
        $error = $request->errorInfo();
        $request->closeCursor();
        if($testReq == true){
            return true;
        }
        else{
            return $error;
        }
    }
}
