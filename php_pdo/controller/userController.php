<?php
class UserController{
    Private $hote;
    Private $db_user;
    Private $db_pass;
    public function __construct($connection){
        $this->hote = $connection['host'];
        $this->db_user = $connection['dbUser'];
        $this->db_pass = $connection['dbPass'];
    }

    Public function db_connect(){
        try{
            $db = new PDO($this->hote,$this->db_user,$this->db_pass);
            return $db;
        }catch(Exception $e){
            header('Location:../view/index.php?erreur_connexion=1');
            exit();
        }
    }
    /**
    * Fonction userLogin
    * Permet la connexion d'un utilisateur
    * @param STRING Pseudonyme de l'utilisateur
    * @param STRING Mot de passe de l'utilisateur
    * @return ARRAY Données de l'utilisateur
    */
    Public function userLogin($pseudo,$password){
        $db = $this->db_connect();
        $params['pseudo'] = (string) $pseudo;
        $params['password'] = (string) $password;

        //Début de la requête
        $request = $db->prepare('   SELECT * FROM utilisateur
                                    WHERE pseudo = :pseudo
                                    AND actif = 1');
        $request->bindValue(':pseudo',$params['pseudo']);
        $testReq = $request->execute();
        $error = $request->errorInfo();
        $data = $request->fetch(PDO::FETCH_ASSOC);
        $request->closeCursor();
        if($testReq == true){
            $checkPassword = password_verify($password,$data['password']);
            if($checkPassword == true){
                return $data;
            }else{
                return 'failLogin';
            }
        }
        else{
            return $error;
        }
    }
    /**
    * Fonction addUser
    * Permet d'ajouter des utilisateurs
    * @param ARRAY données de l'utilisateur
    * @return BOOLEAN
    */
    public function addUser($profil){
        $db = $this->db_connect();
        $request = $db->prepare('  INSERT INTO utilisateur
                            SET pseudo = :pseudo,
                            age = :age,
                            mail = :mail,
                            password = :password,
                            token = :token
                        ');
        $request->bindParam(':pseudo',$profil['pseudo']);
        $request->bindParam(':age',$profil['age']);
        $request->bindParam(':password',$profil['password']);
        $request->bindParam(':token',$profil['token']);
        $request->bindParam(':mail',$profil['mail']);
        $testReq = $request->execute();
        $error = $request->errorInfo();
        $request->closeCursor();
        if($testReq == true){
            return true;
        }
        else{
            return $error;
        }
    }
    public function checkTokens($token){
        $db = $this->db_connect();
        $request = $db->prepare('SELECT id FROM utilisateur WHERE token = :token');
        $request->bindParam(':token',$token);
        $testReq= $request->execute();
        $data = $request->fetch(PDO::FETCH_ASSOC);
        $id = $data['id'];
        $error = $request->errorInfo();
        $request->closeCursor();
        if($testReq == true){
            $tokNull = null;
            $request = $db->prepare('UPDATE utilisateur SET actif = 1 , token = :token WHERE id = :id');
            $request->bindParam(':id',$id);
            $request->bindParam(':token',$tokNull);
            $testReq = $request->execute();
            return $testReq;
        }
        else{
            return $error;
        }
    }
}
