<?php session_start();
$pseudo;
if(!isset($_SESSION['pseudo'])){
    header('Location:index.php');
    exit();
}else{
    $pseudo = $_SESSION['pseudo'];
}
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Tableau de bord</title>
        <?php include('include_html/head.inc.html')?>
    </head>
    <body class="container" id="body">
        <header class="mt-1">
            <a class="btn btn-primary" href="../index.php">Logout</a>
            <div class="text-center">
                <h1>Bienvenue <?php echo  $pseudo?> !</h1>
                <p>Tu te trouve sur le tableau de bord</p>
            </div>
        </header>
        <main>
            <div class="row">
                <!-- Div contenant le formulaire d'ajout -->
                <div class='col-4'>
                    <form method="POST" action="../proposition/propositionGestion.php" class='form-group'>
                        <div>
                            <label for="nomForm">Nom de la proposition</label>
                            <input id="nomForm" type="text"  placeholder="Nom" name="nom" class="form-control" required>
                        </div>
                        <div class="mb-1">
                            <label for="descForm">Description de la proposition</label>
                            <textarea id="descForm" placeholder="Description" name="desc" class="form-control" required></textarea>
                        </div>
                        <input class="btn btn-primary" type="submit" name="addProposition" value="enregistrer">
                    </form>
                </div>
                <!-- Div contenant la liste des proposition existante -->
                <div class='col-6'>
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <th scope='col'>#</th>
                            <th scope="col">Nom</th>
                            <th scope="col">Description</th>
                            <th scope="col">Modifier</th>
                            <th scope="col">Valider</th>
                            <th scope="col">Supprimer</th>
                        </thead>
                        <tbody id='userProposition'>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="container">
                <table class="table table-striped">
                    <thead class="thead-dark">
                        <th>#</th>
                        <th>Nom</th>
                        <th>Affiché</th>
                        <th>Auteur</th>
                        <th>% de vote pour</th>
                        <th>% de vote contre</th>
                    </thead>
                    <tbody id='allProposition'>

                    </tbody>
                </table>
            </div>
        </main>
        <?php include('include_html/javascript.inc.html') ?>
        <script src="../javascript/fetch.js"></script>
        <script src="../javascript/userProposition.js"></script>
        <script src="../javascript/listProposition.js"></script>
    </body>
</html>
