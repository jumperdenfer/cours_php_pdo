<?php session_start();
    if(isset($_GET['erreur_connexion'])){
        $error = $_GET['erreur_connexion'];
        if($error == 0){
            $erreur = "<p>Mauvais login ou mot de passe</p>";
        }
        if($error == 1){
            $erreur = "<p>Erreur de connexion à la base de données</p>";
        }
    }
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Accueil</title>
        <?php include('./include_html/head.inc.html')?>
    </head>
    <body class="container">
        <div class="text-center">
            <h1>Bienvenue sur le site de vote par excellence !</h1>
        </div>
        <?php
            //Affiche une erreur
            if(isset($erreur)){
                echo $erreur;
            }
        ?>
        <!-- Formulaire de connexion d'un utilisateur -->
        <form class="row" method="post" action="../user/traitementLogin.php">
            <div class="form-group col-6">
                <label for="pseudo">Pseudonyme</label>
                <input required class="form-control"  id="pseudo" name="pseudo" type='text' placeholder="Pseudonyme">
            </div>
            <div class="form-group col-6">
                <label for="password">Mot de passe</label>
                <input id="password" required class="form-control" name="password" type="password" placeholder="Mot de passe">
            </div>
            <div class="col">
                <input class="btn btn-primary" type="submit" name="login" value="Connexion">
            </div>
        </form>
        <hr>
        <bouton class="btn btn-primary" data-toggle="modal" data-target="#modal">Inscription</bouton>
        <div id="modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <form class="row" method="post" action="../user/register.php">
                        <div class="form-group col-6">
                            <label for="addPseudo">Pseudonyme</label>
                            <input required class="form-control"  id="addPseudo" name="pseudo" type='text' placeholder="Pseudonyme">
                        </div>
                        <div class="form-group col-6">
                            <label for="addPassword">Mot de passe</label>
                            <input id="addPassword" required class="form-control" name="password" type="password" placeholder="Mot de passe">
                        </div>
                        <div class="form-group col-6">
                            <label for="addEmail">Email</label>
                            <input id="addEmail" required class="form-control" name="mail" type="mail" placeholder="Email">
                        </div>
                        <div class="form-group col-6">
                            <label for="addAge">Age</label>
                            <input id="addAge" required class="form-control" name="age" type="number" placeholder="Age">
                        </div>
                        <div class="col">
                            <input class="btn btn-primary" type="submit" name="register" value="S'enregistrer">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php include('./include_html/javascript.inc.html')?>
    </body>
</html>
