<?php session_start()
//Permet de garder la session active
?>
<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>Proposition</title>
        <?php include('include_html/head.inc.html')?>
    </head>
    <body class="container">
        <header class="mt-1">
            <a class="btn btn-primary" href="dashboard.php">Retour</a>
            <!-- Titre de la proposition-->
            <h1 class="text-center" id="titleProposition">Affichage d'une proposition</h1>
            <div class="col-4">
                <!-- Auteur de la proposition -->
                <p>Auteur: <span id="author">Non trouvée</span></p>
                <!-- Date de création de la proposition -->
                <p>Date de création: <span id="date">10/07/2019</span></p>
                <p>Nombre de vote: <span id="totalVote">0</span></p>
            </div>
            <div class="col-8"></div>
        </header>
        <main class="container">

            <!-- Description de la proposition -->
            <h5>Description:</h5>
            <div id="description">
            </div>
            <!-- Zone des bouton de vote -->
            <div id="boutonArea">
            </div>
            <!-- Zone des commentaire -->
            <h5>Commentaire:</h5>
            <div>
                <div>
                    <form action="../proposition/propositionGestion.php" method="post">
                        <input name="idProposition" type="hidden" value="<?php echo (int) trim(htmlspecialchars($_GET['xtrid'])) ?>">
                        <textarea cols="60" name="commentaire"></textarea>
                        <div>
                            <input name="sendCommentaire" type="submit" value="envoyer">
                        </div>
                    </form>
                </div>
                <div id="commentaires">
                </div>
            </div>
        </main>
        <?php include('include_html/javascript.inc.html') ?>
        <script>
        //Script qui récupère le paramètre passer en get afin de l'utiliser ultérieurement
        function obtenirParametre (sVar) {
            return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
        }
        let idProposition = obtenirParametre('xtrid');
        </script>
        <script src='../javascript/fetch.js'></script>
        <script src="../javascript/showProposition.js"></script>
        <!-- Ajout des fichier de script externe -->
    </body>
</html>
